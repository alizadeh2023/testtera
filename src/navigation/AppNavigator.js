import * as React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import Addtodo from '../screens/Addtodo';
import ListScreen from '../screens/ListScreen';
import TodoListIcon from '../components/TodoListIcon';

const Stack = createStackNavigator()

function MainStackNavigator() {
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen name='Addtodo' component={Addtodo} options={{ headerRight: props => <TodoListIcon {...props} /> }}/>
        <Stack.Screen name='ListScreen' component={ListScreen} />
      </Stack.Navigator>
    </NavigationContainer>
  )
}

export default MainStackNavigator