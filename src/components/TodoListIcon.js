import React from 'react'
import { TouchableOpacity , StyleSheet , View , Text } from 'react-native'
import Icon  from 'react-native-vector-icons/SimpleLineIcons'
import { useNavigation } from '@react-navigation/native'
import { useSelector } from 'react-redux'

function TodoListIcon  () {

  const navigation = useNavigation();
  const cartItems = useSelector(state => state)

  return (
    <TouchableOpacity
      onPress={() => navigation.navigate('ListScreen')}
      style={{ marginRight: 10 }}>
        <View style={styles.itemCountContainer}>
        <Text style={styles.itemCountText}>{cartItems.length}</Text>
      </View>
      <Icon name='list' size={32} color='#101010' />
    </TouchableOpacity>
  )
}

const styles = StyleSheet.create({
  button: {
    marginRight: 10
  },
  itemCountContainer: {
    position: 'absolute',
    height: 25,
    width: 25,
    borderRadius: 15,
    backgroundColor: '#FF7D7D',
    right: 22,
    top: 0,
    alignItems: 'center',
    justifyContent: 'center',
    zIndex: 2000
  },
  itemCountText: {
    color: 'white',
    fontWeight: 'bold'
  }
})

export default TodoListIcon;
