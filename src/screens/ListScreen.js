import React, {useEffect, useState} from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  FlatList,
  TextInput,
  StyleSheet,
  Keyboard
} from 'react-native'
import { useSelector, useDispatch } from 'react-redux';
import { REMOVE_FROM_List  , ADD_TO_List} from '../redux/CartItem';
import Modal from 'react-native-modal';
import Moment from 'moment';

function Separator() {
  return <View style={{ borderBottomWidth: 1, borderBottomColor: '#a9a9a9' }} />
}

function ListScreen() {
  const cartItems = useSelector(state => state)
  const dispatch = useDispatch();
  const [isModalVisible, setModalVisible] = useState(false);
  const [todo, setTodo] = useState('');
  const [itemdo, setItemdo] = useState('');


  const removeItemFromCart = item =>
    dispatch({
      type: REMOVE_FROM_List,
      payload: item
    })


    function updateItemFromCart () {

      let task = {
        id: itemdo.id ,
        task:todo ,
        date:Moment().format("MM-DD-YYYY kk:MM:ss")
      }


     Promise.all([dispatch({ type: REMOVE_FROM_List, payload: itemdo }) , dispatch({ type: ADD_TO_List, payload: task })]) 

      Keyboard.dismiss();
  
      setTodo('');
      setItemdo('');
      setModalVisible(false)

    }
  return (
    <View
      style={{ flex: 1}}>

      {cartItems.length !== 0 ? (
        <FlatList
          data={cartItems}
          keyExtractor={item => item.id.toString()}
          ItemSeparatorComponent={() => Separator()}
          renderItem={({ item }) => (
            <View style={styles.todoItemContainer}>

              <View style={styles.todoItemMetaContainer}>

                <Text style={styles.textDate}>created_at : {item.date}</Text>

                <View style={styles.buttonContainer}>

                  <TouchableOpacity
                    onPress={() => removeItemFromCart(item)}
                    style={styles.buttonRemove}>
                    <Text style={styles.buttonText}>Remove -</Text>
                  </TouchableOpacity>
                  
                  <TouchableOpacity
                    onPress={() => {
                                 setModalVisible(!isModalVisible);
                                 setItemdo(item) 
                                }}
                    style={styles.buttonUpdate}>
                    <Text style={styles.buttonText}>Update title</Text>
                  </TouchableOpacity>

                </View>

              </View>

              <View style={{alignItems:'center' , justifyContent:'space-around' , height:100}}>
              <Text style={styles.titleRight}>
                  Title:
                </Text>
                <Text style={styles.titleRight}>
                  {item.task}
                </Text>
              </View>

            </View>
          )}
        />
      ) : (
        <View style={styles.emptyCartContainer}>
          <Text style={styles.emptyCartMessage}>You have no task :)</Text>
        </View>
      )}

      <Modal onBackButtonPress={() => setModalVisible(false)}	 isVisible={isModalVisible} onBackdropPress={() => setModalVisible(false)}>
        <View style={{height:300 , width:'90%' , backgroundColor:'white' , alignSelf:'center' , justifyContent:'center' , alignItems:'center'}}>
        <TextInput   	editable={true} 	scrollEnabled={false}	style = {styles.TextInputStyles}
              value = {todo}	onChangeText = {(todo) => setTodo(todo)}
              underlineColorAndroid = "transparent"	placeholderStyle={{fontSize:14}}/>
            
            <TouchableOpacity
              onPress={() => updateItemFromCart()}
              style={styles.buttonAdd}>
              <Text style={styles.buttonText}>Add +</Text>
            </TouchableOpacity>
        </View>
      </Modal>

    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff'
  },
  todoItemContainer: {
    flexDirection: 'row',
    padding: 10
  },
  titleRight: {
    width: 100,
    textAlign:'center',
    textAlignVertical:'center' ,
    fontWeight:'bold',
    fontSize:18,
  },
  todoItemMetaContainer: {
    padding: 5,
    paddingLeft: 10,

  },
 
  textDate: {
    fontSize: 15,
    fontWeight: '200',
  },
  buttonContainer: {
    flexDirection:'row' ,
    alignItems:'center',
    height:150
  },
  buttonRemove: {
    borderRadius: 8,
    backgroundColor: '#ff333390',
    padding: 5,
    marginHorizontal:10,

  },
  buttonUpdate: {
    borderRadius: 8,
    backgroundColor: '#24a0ed',
    padding: 5,
    marginHorizontal:10,

  },
  buttonText: {
    fontSize: 18,
    color: '#fff'
  },
  emptyCartContainer: {
    marginTop: 250,
    justifyContent: 'center',
    alignItems: 'center'
  },
  emptyCartMessage: {
    fontSize: 28
  },
   TextInputStyles: {
    textAlign:'center',
    height:40,
    width:'65%',
    alignSelf:'center',
    marginTop:20,
    color:'#450a36',
    borderColor:'blue',
    marginBottom:10,
    borderWidth:1,
    backgroundColor: '#FFF',
    fontSize:15,
    
  },
  buttonAdd:{
    width:'35%' ,
    justifyContent:'center' , 
    alignItems:'center' , 
    borderRadius: 8, 
    backgroundColor: '#24a0ed', 
    padding: 5
  }
})

export default ListScreen