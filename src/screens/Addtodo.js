import React, {useEffect, useState} from 'react';
import {Keyboard , Dimensions ,  View,  Text,  FlatList,  Image,  TouchableOpacity,  StyleSheet , TextInput} from 'react-native'
import { ADD_TO_List  } from '../redux/CartItem.js';
import { useSelector, useDispatch } from 'react-redux'
import Moment from 'moment';

var id =0 ;
  
function Addtodo({navigation}) {
  const dispatch = useDispatch()

  const [todo, setTodo] = useState('');

  useEffect (()=>{


  } , []);

  function addToRedux () {

    alert('Done');

    id++ ;

   let task = {
      id: id ,
      task:todo ,
      date:Moment().format("MM-DD-YYYY kk:MM:ss")
    }

    dispatch({ type: ADD_TO_List, payload: task });

    Keyboard.dismiss();

    setTodo('')

  }


    return (
        <View style={styles.container}>

          <TextInput   	editable={true} 	scrollEnabled={false}	style = {styles.TextInputStyles}
              value = {todo}	onChangeText = {(todo) => setTodo(todo)}
              underlineColorAndroid = "transparent"	placeholderStyle={{fontSize:14}}/>
            
            <TouchableOpacity
              onPress={() => addToRedux()}
              style={styles.buttonAdd}>
              <Text style={styles.buttonText}>Add +</Text>
            </TouchableOpacity>

        </View>
      )
    }

    const styles = StyleSheet.create({
        container: {
          flex: 1,
          backgroundColor: '#fff',
          justifyContent:'center',
          alignItems:'center'
        },
        textTitle: {
          fontSize: 22,
          fontWeight: '400'
        },
        button: {
          borderRadius: 8,
          backgroundColor: '#24a0ed',
          padding: 5
        },
        buttonText: {
          fontSize: 22,
          color: '#fff'
        },
        TextInputStyles: {
          textAlign:'center',
          height:40,
          width:'65%',
          alignSelf:'center',
          marginTop:20,
          color:'#450a36',
          borderColor:'blue',
          marginBottom:10,
          borderWidth:1,
          backgroundColor: '#FFF',
          fontSize:15,
          
        },
        buttonAdd:{
          width:'35%' ,
          justifyContent:'center' , 
          alignItems:'center' , 
          borderRadius: 8, 
          backgroundColor: '#24a0ed', 
          padding: 5
        }
      })

      const { width, height } = Dimensions.get('window');
  
export default Addtodo

