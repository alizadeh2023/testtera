export const ADD_TO_List = 'ADD_TO_List'
export const REMOVE_FROM_List = 'REMOVE_FROM_List'

const initialState = []

const cartItemsReducer = (state = initialState, action) => {
  switch (action.type) {
    case ADD_TO_List:
      return [...state, action.payload]
    case REMOVE_FROM_List:
      return state.filter(cartItem => cartItem.id !== action.payload.id)
  }
  return state
}

export default cartItemsReducer